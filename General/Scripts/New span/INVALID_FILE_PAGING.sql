--――――― ORIGIN ――――――――― USING FILE_ERROR ――――――――――――――――――――――――――――――――――――――――――――――――

SELECT *
FROM (
      select a.*, 
        ROWNUM rnum
      FROM(
            SELECT 
	            "path",
	            "status", 
	            "statusMessage",
	            COUNT(*) OVER() AS TOTAL  
			FROM FILE_ERROR fe
			--${SEARCH}
			ORDER BY "path" 
          )a
      )b
--WHERE b.rnum > ? AND b.rnum <= ?

--――――― FIXED ――――――――― USING FILE_ERROR ――――――――――――――――――――――――――――――――――――――――――――――――

SELECT *
FROM (
      select a.*, 
        ROWNUM rnum
      FROM(
            SELECT 
	            "path",
	            fe."updateTime",
	            "status", 
	            "statusMessage",
	            COUNT(*) OVER() AS TOTAL  
			FROM FILE_ERROR fe
			--${SEARCH}
			ORDER BY fe."updateTime" DESC  
          )a
      )b
--WHERE b.rnum > ? AND b.rnum <= ?

--――――― FIXED ――――――――― USING SPPD_ERROR ――――――――――――――――――――――――――――――――――――――――――――――――

SELECT *
FROM (
      select a.*, 
        ROWNUM rnum
      FROM(
            SELECT 
	            "filePath",
	            "documentDate",
	            "validationResult", 
	            "validationDescription",
	            COUNT(*) OVER() AS TOTAL  
			FROM SPPD_ERROR se
			WHERE "filePath" LIKE UPPER('%520008000990_SP2D_O_20210322_085665_N01.xml%')
			ORDER BY  case when "documentDate" is null then 1 else 0 end, TO_DATE("documentDate", 'YYYY-MM-DD') DESC
          )a
      )b
--WHERE b.rnum > ? AND b.rnum <= ?

SELECT * FROM SPAN_TRACK_RECORD WHERE FILE_NAME = '520008000990_SP2D_O_20210322_142901_N01.xml';