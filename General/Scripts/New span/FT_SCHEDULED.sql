SELECT *
FROM (
      select a.*, 
        ROWNUM rnum
      FROM(
            SELECT 
            		fm."uuid" UUID,
            		fm."app" APPLICATION,
            		fm."sourceType" SOURCE_TYPE,
            		fm."sourceFileName" FILENAME,
            		fm."executionControl",
			fm."valueDate" DOC_DATE,
			fm."sourceId" DOC_NUMBER,
			fm."creditAmount" AMOUNT,
			fm."creditAccount" BENEFICIARY_ACCOUNT,
			fm."creditName" BENEFICIARY_NAME,
			tm.PAYMENT_METHOD_NAME PAYMENT_TYPE,
			tm.PAYMENT_METHOD_ID,
			fm."responseCode" RESPONSE_CODE,
			fm."responseDescription" RESPONSE_MESSAGE,
 			COUNT(*) OVER() AS TOTAL
	     FROM FT_SCHEDULED fm, T_PAYMENT_METHOD tm, T_PROVIDERS tp
	     WHERE tp.PROVIDER_CODE = tm.PROVIDER_CODE
	     AND tm.PAYMENT_METHOD_ID = fm."method1"
	     AND fm."debitAccount" = tp.PROVIDER_ACCOUNT
	     AND tp.PROVIDER_CODE = 'SPN'
	     AND (fm."executionControl" IS NULL
	     OR fm."executionControl" = 'waitForEvent')
	     /*{SEARCH}*/
	     ORDER BY fm."valueDate"
          )a
      )b
WHERE b.rnum > 0 AND b.rnum <= 1000