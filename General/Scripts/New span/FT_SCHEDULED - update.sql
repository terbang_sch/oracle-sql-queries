UPDATE FT_SCHEDULED 
SET	"updateTime" = SYSDATE,
	"attempt" = "attempt" + 1,
	"executionControl" = 'retry'
WHERE "sourceId" = '202101281103035000002'
AND "attempt" = (SELECT MAX("attempt")
				FROM FT_SCHEDULED fs
				WHERE "sourceId" = '202101281103035000002')
				
/*
UPDATE FT_SCHEDULED 
SET	"updateTime" = SYSDATE,
	"attempt" = "attempt" + 1,
	"executionControl" = ?
WHERE "sourceId" = ?
AND "attempt" = (SELECT MAX("attempt")
				FROM FT_SCHEDULED fs
				WHERE "sourceId" = ?)
*/