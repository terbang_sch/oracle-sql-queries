--―― ORIGIN ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― ORIGINAL QUERY ――――――

SELECT *
FROM (
      select a.*, 
        ROWNUM rnum
      FROM(
            SELECT FILE_NAME,
              DOCUMENT_NO,
              BENEFICIARY_ACCOUNT,
              DESCRIPTION,
              AGENT_BANK_ACCOUNT_NAME,
              PAYMENT_METHOD,
              REASON_TO_VOID,
              CREATE_DATE,
		COUNT(*) OVER() AS TOTAL
            FROM SPAN_VOID_DATA_TRX
		${SEARCH}
		ORDER BY CREATE_DATE DESC
          )a
      )b
WHERE b.rnum > ? AND b.rnum <= ?

-- https://www.asciiart.eu/
-- Catto
--
--
--							    /\_____/\
--							   /  =   =  \
--							  ( ≡≡  O  ≡≡ ) Meow 
--							   )         (
--							  (           )
--							 ( (  )   (  ) )
--							(__(__)___(__)__)
--
--
--
--						mew         meong       miaw
--                      /^--^\     /^--^\     /^--^\
--                      \____/     \____/     \____/
--                     /      \   /      \   /      \
--                    |        | |        | |        |
--                     \__  __/   \__  __/   \__  __/
--|^|^|^|^|^|^|^|^|^|^|^|^\ \^|^|^|^/ /^|^|^|^|^\ \^|^|^|^|^|^|^|^|^|^|^|^|
--| | | | | | | | | | | | |\ \| | |/ /| | | | | |\ \| | | | | | | | | | | |
--########################/ /######\ \###########/ /#######################
--| | | | | | | | | | | | \/| | | | \/| | | | | |\/ | | | | | | | | | | | |
--|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|