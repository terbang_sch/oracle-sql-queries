SELECT *
FROM (
      select a.*, 
        ROWNUM rnum
      FROM(
            SELECT 
            DISTINCT FILEPATH, 
            DOCUMENTDATE, 
			COUNT(FILEPATH) AS TOTAL_RECORD,
			SUM(AMOUNT) AS TOTAL_AMOUNT,
			COUNT(*) OVER() AS TOTAL
			FROM SPAN.SPPD se 
--			${SEARCH}
			GROUP BY FILEPATH, DOCUMENTDATE 
			ORDER BY DOCUMENTDATE DESC
          )a
      )b
--WHERE b.rnum > ? AND b.rnum <= ?