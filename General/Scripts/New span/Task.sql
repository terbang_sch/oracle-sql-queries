SELECT
	*
FROM
	(
	SELECT
		a.*,
		ROWNUM rnum
	FROM
		(
		SELECT
			tt.TASK_ID ,
			ama.ACTION_LABEL,
			tt.TASK_USER,
			tt.TASK_DETAILS,
			ama.PROVIDER_CODE,
			tp.PROVIDER_NAME,
			tt.TASK_MENU,
			tt.TASK_ACTION,
			tt.CTL_INS_BY,
			tt.CTL_INS_DTM,
			tt.CTL_UPD_BY,
			tt.CTL_UPD_DTM, 
			COUNT(*) OVER() AS TOTAL
		FROM
			T_TASK tt, ADM_ROLE_APPROVAL ara, ADM_USER_ROLE aur, ADM_MENU_ACTION ama
		LEFT JOIN T_PROVIDERS tp 
		ON ama.PROVIDER_CODE = tp.PROVIDER_CODE 
		WHERE
			ama.MENU_ID = ara.MENU_ID 
			AND ama."ACTION" = ara.ACTION_ID 
			AND tt.TASK_MENU = ama.MENU_ID 
			AND tt.TASK_ACTION = ama."ACTION" 
			AND tt.TASK_MENU = ara.MENU_ID 
			AND tt.TASK_ACTION = ara.ACTION_ID 
			AND aur.ROLE_ID = ara.ROLE_ID  
			AND tt.TASK_LOCK_BY IS NULL 
			AND aur.USER_ID = '2021013000701'
		ORDER BY tt.TASK_ORDER, tt.CTL_INS_DTM  
		)a )b
WHERE
	b.rnum > 0
	AND b.rnum <= 1000