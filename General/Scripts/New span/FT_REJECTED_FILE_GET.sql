--――――― REJECTED FILE PAGING ――――――――――――――― WEB METHOD QUERY ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――

SELECT 	fe."app", 
		fe."sourceFileName", 
		fe."sourceId",
		CASE fe."sourceType"	WHEN 'SPPD' THEN 'SP2D'
								ELSE fe."sourceType" 
		END AS "sourceType",
		fe."creditName", 
		fe."creditBankCode", 
		fe."creditBank", 
		fe."creditAmount", 
		fe."debitAccount", 
		fe."debitName", 
		fe."bankCode", 
		fe."toCurrency", 
		fe."description", 
		tpm.ORI_PAYMENT_METHOD 
FROM FT_ERROR fe
LEFT JOIN T_PROVIDERS tp ON (fe."debitAccount" = tp.PROVIDER_ACCOUNT)
LEFT JOIN T_PAYMENT_METHOD tpm ON (tp.PROVIDER_CODE = tpm.PROVIDER_CODE AND tpm.PAYMENT_METHOD_ID = fe."method1")
WHERE  fe."debitAccount" = ?
AND fe."sourceId" = ?
AND fe."journalSequence" = ?

--――――― REJECTED FILE PAGING ――――――――――――――― IMPLEMENTATION ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――

SELECT 	fe."app", 
		fe."sourceFileName", 
		fe."sourceId", 
		CASE fe."sourceType"	WHEN 'SPPD' THEN 'SP2D'
								ELSE fe."sourceType" 
		END AS "sourceType",
		fe."creditName", 
		fe."creditBankCode", 
		fe."creditBank", 
		fe."creditAmount", 
		fe."debitAccount", 
		fe."debitName", 
		fe."bankCode", 
		fe."toCurrency", 
		fe."description", 
		tpm.ORI_PAYMENT_METHOD 
FROM FT_ERROR fe
LEFT JOIN T_PROVIDERS tp ON (fe."debitAccount" = tp.PROVIDER_ACCOUNT)
LEFT JOIN T_PAYMENT_METHOD tpm ON (tp.PROVIDER_CODE = tpm.PROVIDER_CODE AND tpm.PAYMENT_METHOD_ID = fe."method1")
WHERE  fe."debitAccount" = '1400004255254'
AND fe."sourceId" = '202102280805055000001'
AND fe."journalSequence" = '0007102'

--――――― MY QUERY ――――――――――――――― BACKUP QUERY ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――

SELECT *
FROM FT_ERROR fe
LEFT JOIN T_PROVIDERS tp ON (fe."debitAccount" = tp.PROVIDER_ACCOUNT)
WHERE  fe."debitAccount" = 'SPN'
AND fe."sourceId" = '202003060801010000004'
AND fe."journalSequence" = ''
AND fe."remittanceRef" = ? 