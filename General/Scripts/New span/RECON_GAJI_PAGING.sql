SELECT *
FROM (
      select a.*, 
        ROWNUM rnum
      FROM(
            SELECT 
    	FILE_NAME,
    	DOC_DATE,
    	TOTAL_RECORD,
    	TOTAL_AMOUNT,
    	SOURCE_ACCOUNT_NAME,
COUNT(*) OVER() AS TOTAL
    FROM SPAN_TRACK_FILE stf, T_PROVIDERS tp
 WHERE stf.SOURCE_ACCOUNT = tp.PROVIDER_ACCOUNT
  AND (stf.FILE_NAME NOT IN (SELECT ttd.FILE_NAME FROM TPP.T_TASK tt, TPP.T_TASK_DETAILS ttd WHERE tt.TASK_ID = ttd.TASK_ID AND tt.TASK_STATE = 'CREATED') 
 AND stf.FILE_NAME NOT IN (SELECT ttdh.FILE_NAME FROM TPP.T_TASK_HISTORY tth, TPP.T_TASK_DETAILS_HISTORY ttdh WHERE tth.TASK_ID = ttdh.TASK_ID AND tth.TASK_STATE = 'APPROVED'))
 AND tp.PROVIDER_PROCESS_NAME = 'SC'
 AND stf.FILE_NAME LIKE '%SP2D%'
${SEARCH}
 ORDER BY INSERT_DATE, FILE_NAME DESC
          )a
      )b
WHERE b.rnum > ? AND b.rnum <= 