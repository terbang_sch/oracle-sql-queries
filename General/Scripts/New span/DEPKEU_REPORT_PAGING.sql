-- BETWEEN DATE

SELECT *
FROM (
      select a.*, 
        ROWNUM rnum
      FROM(
         SELECT 
            ft.UUID,
            ft.APP APPLICATION,
            ft.SOURCETYPE,
            ft.SOURCEFILENAME FILENAME,
	     ft.VALUEDATE DOC_DATE,
	     ft.SOURCEID DOC_NUMBER,
	     ft.CREDITAMOUNT AMOUNT,
	     ft.CREDITACCOUNT BENEFICIARY_ACCOUNT,
	     ft.CREDITNAME BENEFICIARY_NAME,
	     tp.PROVIDER_NAME,
	     COUNT(*) OVER() AS TOTAL
            FROM FT ft, T_PROVIDERS tp
	     WHERE ft.DEBITACCOUNT = tp.PROVIDER_ACCOUNT
	     AND TO_DATE(ft.VALUEDATE, 'DDMMYY') BETWEEN TO_DATE('2021-02-09', 'YYYY-MM-DD') AND TO_DATE('2021-02-09', 'YYYY-MM-DD')
	     AND tp.PROVIDER_CODE = 'SPN'
	     ORDER BY ft.VALUEDATE
          )a
      )b
WHERE b.rnum > 0 AND b.rnum <= 1000;


SELECT *
FROM (
      select a.*, 
        ROWNUM rnum
      FROM(
         SELECT 
            ft.UUID,
            ft.APP APPLICATION,
            ft.SOURCETYPE,
            ft.SOURCEFILENAME FILENAME,
	     ft.VALUEDATE DOC_DATE,
	     ft.SOURCEID DOC_NUMBER,
	     ft.CREDITAMOUNT AMOUNT,
	     ft.CREDITACCOUNT BENEFICIARY_ACCOUNT,
	     ft.CREDITNAME BENEFICIARY_NAME,
	     tp.PROVIDER_NAME,
	     COUNT(*) OVER() AS TOTAL
            FROM FT ft, T_PROVIDERS tp
	     WHERE ft.DEBITACCOUNT = tp.PROVIDER_ACCOUNT
	     AND TO_DATE(ft.VALUEDATE, 'DDMMYY') BETWEEN TO_DATE(?, 'YYYY-MM-DD') AND TO_DATE(?, 'YYYY-MM-DD')
	     AND tp.PROVIDER_CODE = ?
	     ORDER BY ft.VALUEDATE
          )a
      )b
WHERE b.rnum > ? AND b.rnum <= ?;

-- EQUALS TO DATE

SELECT *
FROM (
      select a.*, 
        ROWNUM rnum
      FROM(
         SELECT 
            ft.UUID,
            ft.APP APPLICATION,
            ft.SOURCETYPE,
            ft.SOURCEFILENAME FILENAME,
	     ft.VALUEDATE DOC_DATE,
	     ft.SOURCEID DOC_NUMBER,
	     ft.CREDITAMOUNT AMOUNT,
	     ft.CREDITACCOUNT BENEFICIARY_ACCOUNT,
	     ft.CREDITNAME BENEFICIARY_NAME,
	     tp.PROVIDER_NAME,
	     COUNT(*) OVER() AS TOTAL
            FROM FT ft, T_PROVIDERS tp
	     WHERE ft.DEBITACCOUNT = tp.PROVIDER_ACCOUNT
	     AND ft.VALUEDATE = TO_CHAR(TO_DATE('2021-02-09', 'YYYY-MM-DD'), 'DDMMYY')
	     AND tp.PROVIDER_CODE = 'SPN'
	     ORDER BY ft.VALUEDATE
          )a
      )b
WHERE b.rnum > 0 AND b.rnum <= 1000;

-- EQUALS TO DATE - IS

SELECT *
FROM (
      select a.*, 
        ROWNUM rnum
      FROM(
         SELECT 
            ft.UUID,
            ft.APP APPLICATION,
            ft.SOURCETYPE,
            ft.SOURCEFILENAME FILENAME,
	     ft.VALUEDATE DOC_DATE,
	     ft.SOURCEID DOC_NUMBER,
	     ft.CREDITAMOUNT AMOUNT,
	     ft.CREDITACCOUNT BENEFICIARY_ACCOUNT,
	     ft.CREDITNAME BENEFICIARY_NAME,
	     tp.PROVIDER_NAME,
	     COUNT(*) OVER() AS TOTAL
            FROM FT ft, T_PROVIDERS tp
	     WHERE ft.DEBITACCOUNT = tp.PROVIDER_ACCOUNT
	     AND ft.VALUEDATE = TO_CHAR(TO_DATE(?, 'YYYY-MM-DD'), 'DDMMYY')
	     AND tp.PROVIDER_CODE = ?
	     ORDER BY ft.VALUEDATE
          )a
      )b
WHERE b.rnum > ? AND b.rnum <= ?;