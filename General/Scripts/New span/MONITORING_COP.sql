--――――― MONITORING COP ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――

SELECT *
FROM (
      select a.*, 
		ROWNUM rnum,
		COUNT(*) OVER() AS TOTAL
      FROM(
            SELECT 
			frfh."createTime" AS DOC_DATE,
			fe."sourceId" AS DOC_NUMBER,
			fe."creditAccount" AS BENEFICIARY_ACCOUNT,
			fe."creditName" AS BENEFICIARY_NAME,
			fe."debitAccount" AS DEBIT_ACCOUNT,
			fe."debitAmount" AS AMOUNT,
			fe."status" AS STATUS,
			fe."journalSequence" AS JOURNAL_SEQUENCE,
			fe."remittanceRef" AS REMITTANCE_REF,
			0 AS SELECTED
            FROM FT_ERROR fe 
            RIGHT OUTER JOIN FT_REJECTED_FILE_HISTORY frfh ON (
		fe."sourceId" = frfh."sourceId"
            	AND fe."journalSequence" = frfh."journalSequence"
            	AND fe."remittanceRef" = frfh."remittanceRef"
	     )
            JOIN T_PROVIDERS tp ON fe."debitAccount" = tp.PROVIDER_ACCOUNT 
	     JOIN SPAN_TRACK_RECORD str ON fe."sourceFileName" = str.FILE_NAME
	     WHERE tp.PROVIDER_CODE = ?
	     AND frfh."sourceId" IS NULL
	     AND frfh."journalSequence" IS NULL
	     AND frfh."remittanceRef" IS NULL
	     ORDER BY TO_DATE(fe."valueDate", 'DDMMYY')
          )a
	${SEARCH}
      )b
WHERE b.rnum > ? AND b.rnum <= ?